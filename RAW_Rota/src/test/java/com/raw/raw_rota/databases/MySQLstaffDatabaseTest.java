/*
 * Copyright (C) 2019 worralr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.raw.raw_rota.databases;

import interfaces.Istaff;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author worralr
 */
public class MySQLstaffDatabaseTest {
    
    MySQLstaffDatabase instance;
    
    
    public MySQLstaffDatabaseTest() {
        instance = new MySQLstaffDatabase();
        instance.setDatabaseLocation("testStaffDB");
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of AddStaff method, of class MySQLstaffDatabase.
     */
    @Test
    public void testAddStaff() throws Exception {
        System.out.println("AddStaff");
        MySQLstaffDatabase instance = new MySQLstaffDatabase();
        int expResult = 1;
        int result = instance.AddStaff("Keith Richards", "GP");
        assertEquals(expResult, result);
        
    }

    /**
     * Test of getStaffByID method, of class MySQLstaffDatabase.
     */
    @Ignore
    @Test
    public void testGetStaffByID() throws Exception {
        System.out.println("getStaffByID");
        int staffNumber = 0;
        MySQLstaffDatabase instance = new MySQLstaffDatabase();
        Istaff expResult = null;
        Istaff result = instance.getStaffByID(staffNumber);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setDatabaseLocation method, of class MySQLstaffDatabase.
     */
    @Ignore
    @Test
    public void testSetDatabaseLocation() {
        System.out.println("setDatabaseLocation");
        String location = "";
        MySQLstaffDatabase instance = new MySQLstaffDatabase();
        instance.setDatabaseLocation(location);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
