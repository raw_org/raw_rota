/*
 * Copyright (C) 2019 worralr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.raw.raw_rota.databases;

import Exceptions.StaffAlreadyExistsException;
import Exceptions.StaffNotFoundException;
import interfaces.Istaff;
import interfaces.IstaffDatabase;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author worralr
 */
public class MySQLstaffDatabase implements IstaffDatabase {
    
    private Connection conn;
    private String databaseLocation = "staff";
    public MySQLstaffDatabase() {
       // initaliseConnection();
        //checkDatabase();
        
    }
    
    

    @Override
    public int AddStaff(String name, String role) throws StaffAlreadyExistsException {
        try {
            getEmbeddedConnection();
            conn.prepareStatement("INSERT INTO staffDB (name, role) VALUES ('" + name + "','" + role + "';").execute();
            ResultSet executeQuery = conn.prepareStatement("SELECT staffID FROM staffDB where name='" + name + "';").executeQuery();
            return executeQuery.getInt(0);
        } catch (SQLException ex) {
            Logger.getLogger(MySQLstaffDatabase.class.getName()).log(Level.SEVERE, null, ex);
            throw new StaffAlreadyExistsException();
                    
        }
        
    }

    @Override
    public Istaff getStaffByID(int staffNumber) throws StaffNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

//    private void initaliseConnection() {   
//        try {
//            Class.forName("org.apache.derby.jdbc.EmbeddedDriver").newInstance();
//            
//        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
//            Logger.getLogger(MySQLstaffDatabase.class.getName()).log(Level.SEVERE, null, ex);
//        }
//    }

    private void getEmbeddedConnection() throws SQLException {
       Properties props = new Properties();
       props.put("create", "true");
       props.put("user", "rotaManager");
       props.put("password", "password");
                conn = DriverManager.getConnection("jdbc:derby:staffdb;",props);
    }

    private void checkDatabase() {
        try {
            getEmbeddedConnection();
            conn.prepareStatement("describe staff").execute();            
        } catch (SQLException ex) {
            createDatabase();
            Logger.getLogger(MySQLstaffDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void setDatabaseLocation(String location) {
        this.databaseLocation = location;
    }

    private void createDatabase() {
        try {
            getEmbeddedConnection();
            conn.prepareStatement("SOURCE createStaffDB.sql").execute();
        } catch (SQLException ex) {
            Logger.getLogger(MySQLstaffDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
