/* 
 * Copyright (C) 2019 worralr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * Author:  Robert Worrall
 * Created: 02-Jul-2019
 */
CREATE DATABASE staff;
use staff;
CREATE TABLE staffDB (
staffID int(11) NOT NULL AUTOINCRAMENT,
name String NOT NULL UNIQUE,
role String NOT NULL
);

